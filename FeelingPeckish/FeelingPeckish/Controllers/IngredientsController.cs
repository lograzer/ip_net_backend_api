﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using FeelingPeckish.Datas;
using FeelingPeckish.Models;
using FeelingPeckish.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FeelingPeckish.Controllers
{
    [Authorize]
    [ApiController]
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class IngredientsController : ControllerBase
    {
        private readonly FeelingPeckishContext _context;
        private readonly IngredientsService _ingredientsService;
        private readonly UtilisateursService _utilisateursService;

        public IngredientsController(FeelingPeckishContext context, UtilisateursService userService)
        {
            _context = context;
            _ingredientsService = new IngredientsService(_context);         
            _utilisateursService = userService;
        }

        /// <summary>
        /// Get ingredients list for a specific user
        /// </summary>
        /// <param name="OwnerEmail"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<ActionResult<IEnumerable<Ingredient>>> GetIngredientsByOwner([FromQuery]string OwnerEmail)
        {
            try
            {
                if (User.Identity.Name != OwnerEmail)
                {
                    return BadRequest(new { message = "Access forbidden." });
                }
                if (!_utilisateursService.UtilisateurExists(OwnerEmail))
                {
                    throw new Exception();
                }
                return Ok(await _ingredientsService.GetIngredientsByOwner(OwnerEmail));
            }
            catch (Exception)
            {
                return BadRequest(new { message = "An error occured." });
            }
           
        }
        

        /// <summary>
        /// Update full ingredients list for a specific user
        /// </summary>
        /// <param name="Ingredients"></param>
        /// <returns></returns>
        [HttpPut("[action]")]
        public async Task<IActionResult> UpdateIngredientsByOwner([FromBody]ListeIngredient ingredients)
        {
            try
            {
                if (User.Identity.Name != ingredients.OwnerEmail)
                {
                    return BadRequest(new { message = "Access forbidden." });
                }
                if (!_utilisateursService.UtilisateurExists(ingredients.OwnerEmail))
                {
                    throw new Exception();
                }
                return Ok(await _ingredientsService.UpdateIngredientsList(ingredients));
            }
            catch (Exception)
            {
                return BadRequest(new { message = "An error occured." });
            }
        }
    }
}
