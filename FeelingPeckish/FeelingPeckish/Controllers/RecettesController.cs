﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using FeelingPeckish.Datas;
using FeelingPeckish.Models;
using FeelingPeckish.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FeelingPeckish.Controllers
{

    [Authorize]
    [ApiController]
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class RecettesController : ControllerBase
    {
        private readonly FeelingPeckishContext _context;
        private readonly RecettesService _recettesService;
        private readonly UtilisateursService _utilisateursService;

        public RecettesController(FeelingPeckishContext context, UtilisateursService userService)
        {
            _context = context;
            _recettesService = new RecettesService(_context);
            _utilisateursService = userService;
        }

        /// <summary>
        /// Create a new receipt for a specific user
        /// </summary>
        /// <param name="recette"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> PostRecette([FromBody]Recette recette)
        {
            try
            {
                if (User.Identity.Name != recette.UserEmail)
                {
                    return BadRequest(new { message = "Access forbidden." });
                }
                if (!_utilisateursService.UtilisateurExists(recette.UserEmail))
                {
                    throw new Exception();
                }
                return Ok(await _recettesService.CreateRecette(recette));
            }
            catch (Exception)
            {
                return BadRequest(new { message = "An error occured." });
            }

        }

        /// <summary>
        /// Get all receipts from a specific user
        /// </summary>
        /// <param name="OwnerEmail"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetRecettesByOwner([FromQuery]string OwnerEmail)
        {
            try
            {
                if (User.Identity.Name != OwnerEmail)
                {
                    return BadRequest(new { message = "Access forbidden." });
                }
                if (!_utilisateursService.UtilisateurExists(OwnerEmail))
                {
                    throw new Exception();
                }
                return Ok(await _recettesService.GetRecettesByOwner(OwnerEmail));
            }
            catch (Exception)
            {
                return BadRequest(new { message = "An error occured." });
            }

        }

        /// <summary>
        /// Get all receipts for a specific liste of receipts
        /// </summary>
        /// <param name="RecetteListeId"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetRecettesByListe([FromQuery]long RecetteListeId, [FromQuery]string OwnerEmail)
        {
            try
            {
                if (User.Identity.Name != OwnerEmail)
                {
                    return BadRequest(new { message = "Access forbidden." });
                }
                if (!_utilisateursService.UtilisateurExists(OwnerEmail))
                {
                    throw new Exception();
                }
                if (!_recettesService.RecetteListeExists(RecetteListeId, OwnerEmail))
                {
                    return BadRequest(new { message = "List does not exist." });
                }
                return Ok(await _recettesService.GetRecetteListesByOwner(RecetteListeId));
            }
            catch (Exception)
            {
                return BadRequest(new { message = "An error occured." });
            }

        }

        /// <summary>
        /// Update a receipt object
        /// </summary>
        /// <returns></returns>
        [HttpPut("[action]")]
        public async Task<IActionResult> UpdateRecette([FromBody]Recette recette)
        {
            try
            {
                if (User.Identity.Name != recette.UserEmail)
                {
                    return BadRequest(new { message = "Access forbidden." });
                }
                if (!_utilisateursService.UtilisateurExists(recette.UserEmail))
                {
                    throw new Exception();
                }
                return Ok(await _recettesService.UpdateRecette(recette));
            }
            catch (Exception)
            {
                return BadRequest(new { message = "An error occured." });
            }
        }

        /// <summary>
        /// Delete receipt object for a specific user
        /// </summary>
        /// <param name="RecetteId"></param>
        /// <param name="OwnerEmail"></param>
        /// <returns></returns>
        [HttpDelete("[action]")]
        public async Task<IActionResult> DeleteRecette([FromQuery]long RecetteId, [FromQuery]string OwnerEmail)
        {
            try
            {
                if (User.Identity.Name != OwnerEmail)
                {
                    return BadRequest(new { message = "Access forbidden." });
                }
                if (!_utilisateursService.UtilisateurExists(OwnerEmail))
                {
                    throw new Exception();
                }
                if (!_recettesService.RecetteExists(RecetteId))
                {
                    throw new Exception();
                }
                return Ok(await _recettesService.DeleteRecette(RecetteId, OwnerEmail));
            }
            catch (Exception)
            {
                return BadRequest(new { message = "An error occured." });
            }

        }

        /// <summary>
        /// Create a new empty receipts list for a specific user
        /// </summary>
        /// <param name="listeRecettes"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> PostRecetteListe([FromBody]ListeRecette listeRecettes)
        {
            try
            {
                if (User.Identity.Name != listeRecettes.OwnerEmail)
                {
                    return BadRequest(new { message = "Access forbidden." });
                }
                if (!_utilisateursService.UtilisateurExists(listeRecettes.OwnerEmail))
                {
                    throw new Exception();
                }
                return Ok(await _recettesService.CreateRecetteListe(listeRecettes));
            }
            catch (Exception)
            {
                return BadRequest(new { message = "An error occured." });
            }

        }

        /// <summary>
        /// Get all lists of receipts for a specific user
        /// </summary>
        /// <param name="OwnerEmail"></param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetRecetteListes([FromQuery]string OwnerEmail)
        {
            try
            {
                if (User.Identity.Name != OwnerEmail)
                {
                    return BadRequest(new { message = "Access forbidden." });
                }
                if (!_utilisateursService.UtilisateurExists(OwnerEmail))
                {
                    throw new Exception();
                }
                return Ok(await _recettesService.GetRecetteListesByOwner(OwnerEmail));
            }
            catch (Exception)
            {
                return BadRequest(new { message = "An error occured." });
            }

        }

        /// <summary>
        /// Delete a specific list of receipts without deleting the receipts
        /// </summary>
        /// <param name="RecetteListeId"></param>
        /// <returns></returns>
        [HttpDelete("[action]")]
        public async Task<IActionResult> DeleteRecetteListe([FromQuery]long RecetteListeId, [FromQuery]string OwnerEmail)
        {
            try
            {
                if (User.Identity.Name != OwnerEmail)
                {
                    return BadRequest(new { message = "Access forbidden." });
                }
                if (!_utilisateursService.UtilisateurExists(OwnerEmail))
                {
                    throw new Exception();
                }
                if (!_recettesService.RecetteListeExists(RecetteListeId, OwnerEmail))
                {
                    return BadRequest(new { message = "List does not exist." });
                }
                return Ok(await _recettesService.DeleteRecetteListe(RecetteListeId));
            }
            catch (Exception)
            {
                return BadRequest(new { message = "An error occured." });
            }

        }
    }
}