﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FeelingPeckish.Datas;
using FeelingPeckish.Models;
using FeelingPeckish.Services;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Authorization;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Microsoft.Extensions.Configuration;

namespace FeelingPeckish.Controllers
{
    [Authorize]
    [ApiController]
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class UtilisateursController : ControllerBase
    {
        private readonly UtilisateursService _utilisateurService;

        public UtilisateursController(UtilisateursService userService)
        {
            _utilisateurService = userService;
        }

        /// <summary>
        /// Create new user
        /// </summary>
        /// <param name="utilisateur"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost("[action]")]
        public async Task<IActionResult> SignUp([FromForm]Utilisateur utilisateur)
        {
            try
            {
                var token = await _utilisateurService.CreateUtilisateur(utilisateur);
                if (string.IsNullOrEmpty(token.Value))
                {
                    return BadRequest(new { message = "Invalid email or password." });
                }
                return Ok(new { token = token.Value });
            }
            catch (Exception e)
            {
                return BadRequest(new { message = e.Message });
            }

        }

        /// <summary>
        /// Check if log in informations are valid
        /// </summary>
        /// <param name="Email"></param>
        /// <param name="Password"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost("[action]")]
        public async Task<IActionResult> Login([FromForm]Utilisateur utilisateur)
        {
            try
            {
                var token = await _utilisateurService.UtilisateurIsValid(utilisateur);
                if (string.IsNullOrEmpty(token.Value))
                {
                    return BadRequest(new { message = "Invalid credentials." });
                }
                return Ok(new { token = token.Value });
            }
            catch (Exception e)
            {
                return BadRequest(new { message = e.Message });
            }            
        }

        
        /// <summary>
        /// Change user Password 
        /// </summary>
        /// <param name="email"></param>
        /// <param name="oldPassword"></param>
        /// <param name="newPassword"></param>
        /// <returns></returns>
        [HttpPut("[action]")]
        public async Task<IActionResult> UpdatePassword([FromQuery]string email, [FromQuery]string oldPassword, [FromQuery]string newPassword)
        {
            try
            {
                if (User.Identity.Name != email)
                {
                    return BadRequest(new { message = "Access forbidden." });
                }
                var success = await _utilisateurService.UpdatePassword(email, oldPassword, newPassword);
                if (success.Value)
                {
                    return Ok(true);
                }
                else
                {
                    throw new Exception("An error occured.");
                }
            }
            catch (Exception e)
            {
                return BadRequest(new { message = e.Message });
            }
        }
    }
}
