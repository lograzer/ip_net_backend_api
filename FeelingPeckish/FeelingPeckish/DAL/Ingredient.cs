﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace FeelingPeckish.DAL
{
    /// <summary>
    /// A fridge-list-component who represents an available component
    /// </summary>
    public class Ingredient
    {
        /// <summary>
        /// Unique key of the ingredient (match the spoonacular API ID)
        /// </summary>
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long Id { get; set; }
        /// <summary>
        /// Description of the ingredient
        /// </summary>
        [Required]
        public string Name { get; set; }
        /// <summary>
        /// Picture name of the ingredient
        /// </summary>
        public string Picture { get; set; }
        public List<IngredientUtilisateur> IngredientUtilisateurs { get; set; }

    }
}
