﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FeelingPeckish.DAL
{
    public class IngredientUtilisateur
    {
        /// <summary>
        /// Double Primary Key : User ID who saved this Ingredient
        /// </summary>
        [Required]
        public string UtilisateurId { get; set; }
        public Utilisateur Utilisateur { get; set; }

        /// <summary>
        /// Double Primary Key : Ingredient ID saved  by this user
        /// </summary>
        [Required]
        public long IngredientId { get; set; }

        public Ingredient Ingredient { get; set; }
    }
}
