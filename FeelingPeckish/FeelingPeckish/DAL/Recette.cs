﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace FeelingPeckish.DAL
{
    /// <summary>
    /// Favorite Receipt for a user
    /// </summary>
    public class Recette
    {
        /// <summary>
        /// Unique key for a receipt (match spoonacular API ID)
        /// </summary>
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long Id { get; set; }
        /// <summary>
        /// Title of the receipt
        /// </summary>
        [Required]
        public string Title { get; set; }

        public List<RecetteUtilisateur> RecetteUtilisateurs { get; set; }
        
    }
}
