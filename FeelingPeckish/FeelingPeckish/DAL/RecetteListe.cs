﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FeelingPeckish.DAL
{
    public class RecetteListe
    {
        /// <summary>
        /// Generated primary key of the list of receipts
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Name of the receipts list
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Id of the owner of the receipts list
        /// </summary>
        public string OwnerId { get; set; }
        public Utilisateur Owner { get; set; }

        /// <summary>
        /// Liste of receipts belonging to the current list
        /// </summary>
        public List<Recette> Recettes { get; set; }
    }
}
