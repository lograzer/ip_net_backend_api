﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FeelingPeckish.DAL
{
    public class RecetteUtilisateur
    {
        /// <summary>
        /// Double Primary Key : User ID who saved this Receipt as favorite
        /// </summary>
        [Required]
        public string UtilisateurId { get; set; }

        public Utilisateur Utilisateur { get; set; }

        /// <summary>
        /// Double Primary Key : Receipt ID saved  by this user
        /// </summary>
        [Required]
        public long RecetteId { get; set; }

        public Recette Recette { get; set; }

        /// <summary>
        /// Related liste for that set user + receipt
        /// </summary>
        public long? RecetteListeId { get; set; }

        public RecetteListe RecetteListe { get; set; }

    }
}
