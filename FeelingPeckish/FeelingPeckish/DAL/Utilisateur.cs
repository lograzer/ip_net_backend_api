﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FeelingPeckish.DAL
{
    /// <summary>
    /// A simple user account
    /// </summary>
    public class Utilisateur
    {
        /// <summary>
        /// Unique Email address for sign in
        /// </summary>
        [Key]
        [Required]
        public string Email { get; set; }
        /// <summary>
        /// Hashed Password for sign in
        /// </summary>
        [Required]
        public string Password { get; set; }

        public List<IngredientUtilisateur> IngredientUtilisateurs { get; set; }

        public List<RecetteUtilisateur> RecetteUtilisateurs { get; set; }

        public List<RecetteListe> RecetteListes { get; set; }

    }
}
