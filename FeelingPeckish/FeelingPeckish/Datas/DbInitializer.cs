﻿using FeelingPeckish.DAL;
using FeelingPeckish.Tool;
using System.Linq;

namespace FeelingPeckish.Datas
{
    public class DbInitializer
    {
        public static void Initialize(FeelingPeckishContext context)
        {
            context.Database.EnsureCreated();

            // Look for any users.
            if (context.Utilisateurs.Any())
            {
                return;   // DB has been seeded
            }

            // Users
            string hash = PasswordHasherHelper.Hash("root", 10);
            var users = new Utilisateur[]
            {
                new Utilisateur{Email="lou@root.fr", Password=hash},
                new Utilisateur{Email="louise@root.fr", Password=hash}
            };
            foreach (Utilisateur u in users)
            {
                context.Utilisateurs.Add(u);
            }
            context.SaveChanges();

            // Ingredients
            var ingredients = new Ingredient[]
            {
                new Ingredient{ Id=9003, Name="apple", Picture="apple.jpg" },
                new Ingredient{ Id=9019, Name="applesauce", Picture="applesauce.png" },
                new Ingredient{ Id=1009016, Name="apple cider", Picture="apple-cider.jpg" }
            };
            foreach (Ingredient i in ingredients)
            {
                context.Ingredients.Add(i);
            }
            context.SaveChanges();
                                   
            // Receipts
            var receipts = new Recette[]
            {
                new Recette{ Id=633508, Title="Baked Cheese Manicotti"},
                new Recette{ Id=634873, Title="Best Baked Macaroni and Cheese"}
            };
            foreach (Recette r in receipts)
            {
                context.Recettes.Add(r);
            }
            context.SaveChanges();
            
            // Users - Ingredients
            var userIngredients = new IngredientUtilisateur[]
            {
                new IngredientUtilisateur{ UtilisateurId="lou@root.fr", IngredientId=9003 },
                new IngredientUtilisateur{ UtilisateurId="lou@root.fr", IngredientId=9019 },
                new IngredientUtilisateur{ UtilisateurId="lou@root.fr", IngredientId=1009016 },
                new IngredientUtilisateur{ UtilisateurId="louise@root.fr", IngredientId=9003 },
                new IngredientUtilisateur{ UtilisateurId="louise@root.fr", IngredientId=9019 }
            };
            foreach (IngredientUtilisateur i in userIngredients)
            {
                context.IngredientUtilisateurs.Add(i);
            }
            context.SaveChanges();

            // Receipts lists
            var receiptLists = new RecetteListe[]
            {
                new RecetteListe{ Title="Italian Food", OwnerId="lou@root.fr"},
                new RecetteListe{ Title="Junk Food", OwnerId="lou@root.fr"},
                new RecetteListe{ Title="Plats italiens", OwnerId="louise@root.fr"}
            };
            foreach (RecetteListe rl in receiptLists)
            {
                context.RecetteListes.Add(rl);
            }
            context.SaveChanges();

            // Users - Receipts
            var userReceipts = new RecetteUtilisateur[]
            {
                new RecetteUtilisateur{ UtilisateurId="lou@root.fr", RecetteId=633508, RecetteListeId=1 },
                new RecetteUtilisateur{ UtilisateurId="lou@root.fr", RecetteId=634873, RecetteListeId=1 },
                new RecetteUtilisateur{ UtilisateurId="louise@root.fr", RecetteId=633508, RecetteListeId=3 },
                new RecetteUtilisateur{ UtilisateurId="louise@root.fr", RecetteId=634873 }
            };
            foreach (RecetteUtilisateur r in userReceipts)
            {
                context.RecetteUtilisateurs.Add(r);
            }
            context.SaveChanges(); 

        }
    }
}
