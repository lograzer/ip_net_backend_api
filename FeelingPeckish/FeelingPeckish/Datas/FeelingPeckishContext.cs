﻿using FeelingPeckish.DAL;
using Microsoft.EntityFrameworkCore;

namespace FeelingPeckish.Datas
{
    /// <summary>
    /// Context for requesting and updating the Database
    /// </summary>
    public class FeelingPeckishContext : DbContext
    {
        public FeelingPeckishContext(DbContextOptions<FeelingPeckishContext> options) : base(options)
        {
        }

        // Tables
        public DbSet<Utilisateur> Utilisateurs { get; set; }
        public DbSet<Ingredient> Ingredients { get; set; }
        public DbSet<Recette> Recettes { get; set; }
        public DbSet<IngredientUtilisateur> IngredientUtilisateurs { get; set; }
        public DbSet<RecetteUtilisateur> RecetteUtilisateurs { get; set; }
        public DbSet<RecetteListe> RecetteListes { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Double primary key
            modelBuilder.Entity<IngredientUtilisateur>()
            .HasKey(i => new { i.IngredientId, i.UtilisateurId });
            
            modelBuilder.Entity<RecetteUtilisateur>()
            .HasKey(r => new { r.RecetteId, r.UtilisateurId });
            
            // Foreign Keys
            // IngredientUtilisateurs -> Ingredient
            modelBuilder.Entity<IngredientUtilisateur>()
            .HasOne(iu => iu.Ingredient)
            .WithMany(i => i.IngredientUtilisateurs)
            .HasForeignKey(ui => ui.IngredientId);

            modelBuilder.Entity<IngredientUtilisateur>()
            .HasOne(iu => iu.Utilisateur)
            .WithMany(u => u.IngredientUtilisateurs)
            .HasForeignKey(ui => ui.UtilisateurId);

            // RecetteUtilisateurs -> Recette
            modelBuilder.Entity<RecetteUtilisateur>()
            .HasOne(ru => ru.Recette)
            .WithMany(u => u.RecetteUtilisateurs)
            .HasForeignKey(ru => ru.RecetteId);

            modelBuilder.Entity<RecetteUtilisateur>()
           .HasOne(ru => ru.Utilisateur)
           .WithMany(u => u.RecetteUtilisateurs)
           .HasForeignKey(ru => ru.UtilisateurId);

            // RecetteList -> Utilisateur (Owner)
            modelBuilder.Entity<RecetteListe>()
            .HasOne(rl => rl.Owner)
            .WithMany(u => u.RecetteListes)
            .HasForeignKey(rl => rl.OwnerId);
        }
    }
}