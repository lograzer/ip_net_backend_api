﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FeelingPeckish.Models
{
    public class ListeIngredient
    {
        /// <summary>
        /// Owner of the list of ingredients
        /// </summary>
        [Required]
        public string OwnerEmail { get; set; }

        /// <summary>
        /// User ID who created this list
        /// </summary>
        [Required]
        public IEnumerable<Ingredient> Ingredients { get; set; }
    }
}
