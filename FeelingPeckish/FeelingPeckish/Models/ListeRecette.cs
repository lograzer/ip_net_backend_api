﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FeelingPeckish.Models
{
    public class ListeRecette
    {
        public long? Id { get; set; }

        /// <summary>
        /// Owner of the list
        /// </summary>
        [Required]
        public string OwnerEmail { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        public string Title { get; set; }
    }
}
