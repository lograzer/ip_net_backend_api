﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FeelingPeckish.Models
{
    /// <summary>
    /// Favorite Receipt for a user
    /// </summary>
    public class Recette
    {
        /// <summary>
        /// Unique key for a receipt (match spoonacular API ID)
        /// </summary>
        /// 
        [Required]
        public long Id { get; set; }
        /// <summary>
        /// Title of the receipt
        /// </summary>
        [Required]
        public string Title { get; set; }
        /// <summary>
        /// User ID who saved this Receipt as favorite
        /// </summary>
        [Required]
        public string UserEmail { get; set; }
        /// <summary>
        /// Id of the receipt list to which the receipt need to be added
        /// </summary>
        public long? MyListeId{ get; set; }

    }
}
