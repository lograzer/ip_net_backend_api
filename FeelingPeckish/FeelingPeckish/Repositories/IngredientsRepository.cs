﻿using FeelingPeckish.Datas;
using FeelingPeckish.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FeelingPeckish.Repositories
{
    public class IngredientsRepository
    {
        private readonly FeelingPeckishContext _context;

        public IngredientsRepository(FeelingPeckishContext context)
        {
            _context = context;
        }

        public async Task<ActionResult<IEnumerable<Ingredient>>> GetIngredientsByOwner(string ownerEmail)
        {
            List<Ingredient> results = new List<Ingredient>();
            var ingredientIds = await _context.IngredientUtilisateurs.Where(r => r.UtilisateurId == ownerEmail).Select(r => r.IngredientId).ToListAsync();
            var ingredients = await _context.Ingredients.Where(i => ingredientIds.Contains(i.Id)).ToListAsync();

            if (ingredients != null)
            {
                ingredients.ForEach(i => results.Add(
                    new Ingredient
                    {
                        Id = i.Id,
                        Name = i.Name,
                        Picture = i.Picture
                    }
                ));
            }
            return results;
        }

        public async Task<ActionResult<bool>> UpdateIngredientsList(ListeIngredient liste)
        {
            try
            {
                // Remove old list of this user
                if (!DeleteIngredientsList(liste.OwnerEmail).Result.Value)
                {
                    throw new Exception("Failed to remove old ingredients list.");
                }

                var ingredients = liste.Ingredients.Where(i => i.Id != 0);
                if (ingredients.Any())
                {
                    // Update new ingredients
                    if (!UpdateIngredients(liste.Ingredients).Result.Value)
                    {
                        throw new Exception("Failed to update ingredient item informations");
                    }
                    // Add new list for this user
                    return await CreateIngredientsList(liste);
                }
                else
                {
                    return true;
                }
            }
            catch (Exception e)
            {
                throw new DbUpdateException(e.Message);
            }

        }
        
        public async Task<ActionResult<bool>> CreateIngredientsList(ListeIngredient liste)
        {
            try
            {
                liste.Ingredients.Where(i => i.Id != 0).ToList().ForEach(i => _context.IngredientUtilisateurs.Add(
                    new DAL.IngredientUtilisateur
                    {
                        IngredientId = i.Id,
                        UtilisateurId = liste.OwnerEmail
                    }
                ));

                return await _context.SaveChangesAsync() == liste.Ingredients.Count();
            }
            catch (Exception)
            {
                throw new ArgumentException("Update list failed.");
            }
        }

        private async Task<ActionResult<bool>> UpdateIngredients(IEnumerable<Ingredient> newIngredients)
        {
            try
            { 
                // Get existing ingredients
                var existingIngredients = _context.Ingredients.Where(i => newIngredients.Select(e => e.Id).Contains(i.Id));
                // If no update to make
                if (existingIngredients.Count() == newIngredients.Count())
                {
                    return true;
                }
                // If update to make
                newIngredients
                    .Where(e => !existingIngredients.Select(x => x.Id).Contains(e.Id) && e.Id != 0)
                    .ToList().ForEach(n => _context.Ingredients.Add(new DAL.Ingredient { Id=n.Id, Name=n.Name, Picture=n.Picture })             
                );
                if (_context.ChangeTracker.HasChanges())
                {
                    return await _context.SaveChangesAsync() > 0;
                }
                return false;
            }
            catch (Exception)
            {
                throw new DbUpdateException("Wrong ingredient informations.");
            }
        }

        private async Task<ActionResult<bool>> DeleteIngredientsList(string ownerEmail)
        {
            try
            {
                int nbObjects = 0;
                var lines = _context.IngredientUtilisateurs.Where(i => i.UtilisateurId == ownerEmail).ToList();
                nbObjects = lines.Count();
                _context.RemoveRange(lines);
                return await _context.SaveChangesAsync() == nbObjects;
            }
            catch (Exception)
            {
                throw new DbUpdateException();
            }
        }
    }
}
