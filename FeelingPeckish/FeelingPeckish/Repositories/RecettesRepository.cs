﻿using FeelingPeckish.Datas;
using FeelingPeckish.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FeelingPeckish.Repositories
{
    public class RecettesRepository
    {
        private readonly FeelingPeckishContext _context;

        public RecettesRepository(FeelingPeckishContext context)
        {
            _context = context;
        }

        public async Task<ActionResult<IEnumerable<Recette>>> GetRecettesByOwner(string ownerEmail)
        {
            List<Recette> results = new List<Recette>();
            var recetteIds = await _context.RecetteUtilisateurs.Where(r => r.UtilisateurId == ownerEmail).Select(r => r.RecetteId).ToListAsync();
            var recettes = await _context.Recettes.Where(i => recetteIds.Contains(i.Id)).ToListAsync();

            if (recettes != null)
            {
                foreach (DAL.Recette r in recettes)
                {
                    var newObject = new Recette
                    {
                        Id = r.Id,
                        Title = r.Title,
                        UserEmail = ownerEmail
                    };

                    newObject.MyListeId = GetListeId(r.Id, ownerEmail);

                    results.Add(newObject);
                }
            }
            return results;
        }

        public async Task<ActionResult<IEnumerable<ListeRecette>>> GetRecetteListeByOwner(string ownerEmail)
        {
            List<ListeRecette> results = new List<ListeRecette>();
            var listeIds = await _context.RecetteListes.Where(r => r.OwnerId == ownerEmail).Select(r => r.Id).ToListAsync();
            var listes = await _context.RecetteListes.Where(i => listeIds.Contains(i.Id)).ToListAsync();

            if (listes != null)
            {
                listes.ForEach(r => results.Add(
                    new ListeRecette
                    {
                        OwnerEmail = ownerEmail,
                        Title = r.Title,
                        Id = r.Id
                    }
                ));
            }
            return results;
        }

        public async Task<ActionResult<IEnumerable<Recette>>> GetRecettesByListe(long recetteListeId)
        {
            List<Recette> results = new List<Recette>();
            var recetteIds = await _context.RecetteUtilisateurs.Where(r => r.RecetteListeId == recetteListeId).Select(r => r.RecetteId).ToListAsync();
            var recettes = await _context.Recettes.Where(i => recetteIds.Contains(i.Id)).ToListAsync();

            if (recettes != null)
            {
                foreach (DAL.Recette r in recettes)
                {
                    var newObject = new Recette
                    {
                        Id = r.Id,
                        Title = r.Title
                    };

                    newObject.MyListeId = recetteListeId;
                    results.Add(newObject);
                }
            }
            return results;
        }

        public async Task<ActionResult<bool>> CreateRecette(Recette recette)
        {
            try
            {
                var receipt = await _context.Recettes.Where(r => r.Id == recette.Id).FirstOrDefaultAsync();
                if (receipt == null)
                {
                    DAL.Recette newRecette = new DAL.Recette
                    {
                        Id = recette.Id,
                        Title = recette.Title
                    };
                    _context.Recettes.Add(newRecette);
                    return await _context.SaveChangesAsync() > 0;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception)
            {
                throw new DbUpdateException();
            }
        }

        public async Task<ActionResult<bool>> AddRecetteToUtilisateur(Recette recette)
        {
            try
            {
                var receipt = await _context.RecetteUtilisateurs.Where(r => r.RecetteId == recette.Id && r.UtilisateurId == recette.UserEmail).FirstOrDefaultAsync();
                if (receipt == null)
                {
                    DAL.RecetteUtilisateur newRecette = new DAL.RecetteUtilisateur
                    {
                        RecetteId = recette.Id,
                        UtilisateurId = recette.UserEmail
                    };
                    if (recette.MyListeId.HasValue && recette.MyListeId.Value != 0)
                    {
                        newRecette.RecetteListeId = recette.MyListeId.Value;
                    }
                    _context.RecetteUtilisateurs.Add(newRecette);
                    return await _context.SaveChangesAsync() > 0;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception)
            {
                throw new DbUpdateException();
            }
        }

        public async Task<ActionResult<bool>> CreateRecetteListe(ListeRecette recetteListe)
        {
            try
            {
                var receiptListe = await _context.RecetteListes.Where(r => r.OwnerId == recetteListe.OwnerEmail && r.Title == recetteListe.Title).FirstOrDefaultAsync();
                if (receiptListe == null)
                {
                    DAL.RecetteListe newList = new DAL.RecetteListe
                    {
                        OwnerId = recetteListe.OwnerEmail,
                        Title = recetteListe.Title
                    };
                    _context.RecetteListes.Add(newList);
                    return await _context.SaveChangesAsync() > 0;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception)
            {
                throw new DbUpdateException();
            }
        }

        public async Task<ActionResult<bool>> UpdateRecette(Recette recette)
        {
            try
            {
                var receipt = _context.RecetteUtilisateurs.Where(u => u.RecetteId == recette.Id && u.UtilisateurId == recette.UserEmail).FirstOrDefault();
                if (receipt != null)
                {
                    if (recette.MyListeId.HasValue)
                    {
                        receipt.RecetteListeId = recette.MyListeId.Value;
                    }
                    else
                    {
                        receipt.RecetteListeId = null;
                    }
                    if (_context.ChangeTracker.HasChanges())
                    {
                        return await _context.SaveChangesAsync() > 0;
                    }
                    else
                    {
                        return true;
                    }
                }
                else
                {
                    return true;
                }

            }
            catch (Exception e)
            {
                throw new DbUpdateException(e.Message);
            }
        }

        public async Task<ActionResult<bool>> DeleteRecette(long recetteId, string ownerEmail)
        {
            try
            {
                var receipt = _context.RecetteUtilisateurs.Where(r => r.RecetteId == recetteId && r.UtilisateurId == ownerEmail).FirstOrDefault();
                _context.Remove(receipt);
                return await _context.SaveChangesAsync() > 0;
            }
            catch (Exception)
            {
                throw new DbUpdateException();
            }
        }

        public async Task<ActionResult<bool>> DeleteRecetteListe(long recetteListId)
        {
            try
            {
                var receiptsList = _context.RecetteListes.Where(rl => rl.Id == recetteListId).FirstOrDefault();
                _context.Remove(receiptsList);
                return await _context.SaveChangesAsync() > 0;
            }
            catch (Exception)
            {
                throw new DbUpdateException();
            }
        }

        public bool RecetteExists(long id)
        {
            return _context.Recettes.Any(r => r.Id == id);
        }

        public bool RecetteListeExists(long id, string owner)
        {
            return _context.RecetteListes.Any(rl => rl.Id == id && rl.OwnerId == owner);
        }

        private long? GetListeId(long recetteId, string ownerEmail)
        {
            return _context.RecetteUtilisateurs
                .Where(ru => ru.RecetteId == recetteId && ru.UtilisateurId == ownerEmail)
                .Select(ru => ru.RecetteListeId)
                .FirstOrDefault();
        }
    }
}
