﻿using FeelingPeckish.Datas;
using FeelingPeckish.Models;
using FeelingPeckish.Tool;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FeelingPeckish.Repositories
{
    public class UtilisateursRepository
    {
        private readonly FeelingPeckishContext _context;
        public UtilisateursRepository(FeelingPeckishContext context)
        {
            _context = context;
        }

        public async Task<ActionResult<IEnumerable<Utilisateur>>> GetUtilisateurs()
        {
            List<Utilisateur> results = new List<Utilisateur>();
            var users = await _context.Utilisateurs.ToListAsync();
            if(users != null)
            {
                users.ForEach(u => results.Add(
                    new Utilisateur
                    {
                        Email = u.Email,
                        Password = u.Password
                    }
                ));
            }
            return results;
        }

        public async Task<ActionResult<Utilisateur>> GetUtilisateur(string email)
        {
            Utilisateur result = null;

            var user =  await _context.Utilisateurs.Where(u => u.Email == email).FirstOrDefaultAsync();
            if (user != null)
            {
                result = new Utilisateur { Email = user.Email, Password = user.Password };
            }
            return result;
        }

        public async Task<ActionResult<bool>> CreateUtilisateur(Utilisateur utilisateur)
        {
            try
            {
                DAL.Utilisateur newUtilisateur = new DAL.Utilisateur
                {
                    Email = utilisateur.Email,
                    Password = PasswordHasherHelper.Hash(utilisateur.Password)
                };
                _context.Utilisateurs.Add(newUtilisateur);
                return await _context.SaveChangesAsync() > 0;
            }
            catch (Exception)
            {
                throw new DbUpdateException();
            }
        }
    
        public async Task<ActionResult<bool>> UpdatePassword(string email, string newPassword)
        {   try
            {
                var utilisateur = _context.Utilisateurs.Where(u => u.Email == email).FirstOrDefault();
                utilisateur.Password = PasswordHasherHelper.Hash(newPassword);
                return await _context.SaveChangesAsync() > 0;
            }
            catch (Exception)
            {
                throw new DbUpdateException();
            }
            
        }

        public bool PasswordExists(String email, String oldPassword)
        {
            var utilisateur = _context.Utilisateurs.Where(u => u.Email == email).FirstOrDefault();

            return PasswordHasherHelper.Verify(oldPassword, utilisateur.Password);
        }

        public bool UtilisateurExists(string id)
        {
            return _context.Utilisateurs.Any(e => e.Email == id);
        }

    }
}
