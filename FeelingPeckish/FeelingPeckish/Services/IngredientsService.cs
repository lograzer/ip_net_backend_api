﻿using FeelingPeckish.Datas;
using FeelingPeckish.Models;
using FeelingPeckish.Repositories;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FeelingPeckish.Services
{
    public class IngredientsService
    {
        private readonly FeelingPeckishContext _context;
        private readonly IngredientsRepository _ingredientRepository;

        public IngredientsService(FeelingPeckishContext context)
        {
            _context = context;
            _ingredientRepository = new IngredientsRepository(_context);
        }

        public async Task<ActionResult<IEnumerable<Ingredient>>> GetIngredientsByOwner(string ownerEmail)
        {
           return await _ingredientRepository.GetIngredientsByOwner(ownerEmail);
        }

        public async Task<ActionResult<bool>> UpdateIngredientsList(ListeIngredient liste)
        {
            return await _ingredientRepository.UpdateIngredientsList(liste);
        }

    }
}
