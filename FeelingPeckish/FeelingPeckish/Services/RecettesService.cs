﻿using FeelingPeckish.Datas;
using FeelingPeckish.Models;
using FeelingPeckish.Repositories;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FeelingPeckish.Services
{
    public class RecettesService
    {
        private readonly FeelingPeckishContext _context;
        private readonly RecettesRepository _recettesRepository;

        public RecettesService(FeelingPeckishContext context)
        {
            _context = context;
            _recettesRepository = new RecettesRepository(_context);
        }

        public async Task<ActionResult<IEnumerable<Recette>>> GetRecettesByOwner(string ownerEmail)
        {
            return await _recettesRepository.GetRecettesByOwner(ownerEmail);
        }

        public async Task<ActionResult<IEnumerable<ListeRecette>>> GetRecetteListesByOwner(string ownerEmail)
        {
            return await _recettesRepository.GetRecetteListeByOwner(ownerEmail);
        }

        public async Task<ActionResult<IEnumerable<Recette>>> GetRecetteListesByOwner(long recetteListeId)
        {
            return await _recettesRepository.GetRecettesByListe(recetteListeId);
        }

        public async Task<ActionResult<bool>> CreateRecette(Recette recette)
        {
            try
            {
                await _recettesRepository.CreateRecette(recette);

                return await _recettesRepository.AddRecetteToUtilisateur(recette);
            }
            catch (Exception)
            {
                throw new Exception();
            }
        }

        public async Task<ActionResult<bool>> CreateRecetteListe(ListeRecette liste)
        {
            try
            {
                return await _recettesRepository.CreateRecetteListe(liste);
            }
            catch (Exception)
            {
                throw new Exception();
            }
        }

        public async Task<ActionResult<bool>> UpdateRecette(Recette recette)
        {
            try
            {
                return await _recettesRepository.UpdateRecette(recette);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public async Task<ActionResult<bool>> DeleteRecette(long recetteListId, string OwnerEmail)
        {
            try
            {
                return await _recettesRepository.DeleteRecette(recetteListId, OwnerEmail);
            }
            catch (Exception)
            {
                throw new Exception();
            }
        }

        public async Task<ActionResult<bool>> DeleteRecetteListe(long recetteListId)
        {
            try
            {
                var receipts = await _recettesRepository.GetRecettesByListe(recetteListId);
                if (receipts.Value.Any())
                {
                    foreach (Recette r in receipts.Value)
                    {
                        r.MyListeId = null;
                        await _recettesRepository.UpdateRecette(r);
                    }
                }
                return await _recettesRepository.DeleteRecetteListe(recetteListId);
            }
            catch (Exception)
            {
                throw new Exception();
            }
        }

        public bool RecetteExists(long id)
        {
            return _recettesRepository.RecetteExists(id);
        }

        public bool RecetteListeExists(long id, string owner)
        {
            return _recettesRepository.RecetteListeExists(id, owner);
        }
    }
}
