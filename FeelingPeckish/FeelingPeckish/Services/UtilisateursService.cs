﻿using FeelingPeckish.Datas;
using FeelingPeckish.Helpers;
using FeelingPeckish.Models;
using FeelingPeckish.Repositories;
using FeelingPeckish.Tool;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace FeelingPeckish.Services
{
    public class UtilisateursService
    {
        private readonly FeelingPeckishContext _context;
        private readonly UtilisateursRepository _userRepository;
        private readonly AppSettings _appSettings;

        public UtilisateursService(FeelingPeckishContext context, IOptions<AppSettings> appSettings)
        {
            _context = context;
            _userRepository = new UtilisateursRepository(_context);
            _appSettings = appSettings.Value;
        }

        public async Task<ActionResult<IEnumerable<Utilisateur>>> GetUtilisateurs()
        {
            return await _userRepository.GetUtilisateurs();
        }

        public async Task<ActionResult<Utilisateur>> GetUtilisateur(string email)
        {
            return await _userRepository.GetUtilisateur(email);
        }

        public async Task<ActionResult<String>> CreateUtilisateur(Utilisateur utilisateur)
        {
            if (!IsValidEmail(utilisateur.Email))
            {
                throw new ArgumentException("Invalid email address format.");
            }
            if (UtilisateurExists(utilisateur.Email))
            {
                throw new ArgumentException("Email address already used.");
            }
            try
            {
                var success = await _userRepository.CreateUtilisateur(utilisateur);
                if (!success.Value)
                {
                    throw new Exception();
                }
            }
            catch (Exception)
            {
                throw new Exception("An error occured.");
            }
            return GetTokenFromAuthentification(utilisateur);
        }

        public async Task<ActionResult<bool>> UpdatePassword(string email, string oldPassword, string newPassword)
        {
            if (!UtilisateurExists(email))
            {
                throw new NullReferenceException("Use does not exist.");
            }
            if (!PasswordExists(email, oldPassword))
            {
                throw new ArgumentException("Wrong password");
            }
            try
            {
                return await _userRepository.UpdatePassword(email, newPassword);
            }
            catch (Exception)
            {
                throw new DbUpdateException();
            }

        }

        public async Task<ActionResult<String>> UtilisateurIsValid(Utilisateur user)
        {
            try
            {
                if (!UtilisateurExists(user.Email))
                {
                    throw new Exception();
                }
                var utilisateur = await _userRepository.GetUtilisateur(user.Email);
                if(utilisateur == null)
                {
                    throw new Exception();
                }
                bool isValid = utilisateur.Value.Email == user.Email && PasswordHasherHelper.Verify(user.Password, utilisateur.Value.Password);
                if (!isValid)
                {
                    throw new Exception();   
                }
            }
            catch (Exception)
            {
                throw new Exception("Invalid credentials.");
            }
            return GetTokenFromAuthentification(user);
        }

        public String GetTokenFromAuthentification(Utilisateur user)
        {
           
            // authentication successful so generate jwt token
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.Email.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            String tokenResult = tokenHandler.WriteToken(token);

            return tokenResult;
        }

        private bool PasswordExists(String email, String oldPassword)
        {
            return _userRepository.PasswordExists(email, oldPassword);
        }

        public bool UtilisateurExists(string email)
        {
            return _userRepository.UtilisateurExists(email);
        }

        private bool IsValidEmail(string email)
        {
            try
            {
                var res = new System.Net.Mail.MailAddress(email);
                return res.Address == email;
            }
            catch
            {
                return false;
            }
        }
    }
}
