# Feeling Peckish API 
- **Auteures** : Louise GRÄZER et Lou GRUNNER  
- **Lien vers l'API** : Disponible avec une [Interface Swagger](http://feelingpeckish3.azurewebsites.net/swagger/index.html)  

### Développée avec :
- C# Asp.NET Core 3.1 + SqlServer  

### Hébergée sur :
- Microsoft Azure

### Cette API du site "Feeling Peckish" propose les services suivants :
- Créer et mettre à jour des utilisateurs
- Créer et mettre à jour des ingredients pour un utilisateur
- Créer et mettre à jour des recettes favorites pour un utilisateur

### L'API propose également les fonctionnalités suivantes :
- Authorisation Bearer Jwt pour accéder aux endpoints
- Interface Swagger avec documentation de l'API